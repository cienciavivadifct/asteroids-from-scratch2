from panda3d.core import NodePath
from panda3d.core import LPoint2, LVector2
from panda3d.core import LPoint3
from panda3d.core import TransparencyAttrib

class GameActor(NodePath):
    def __init__(self, 
                 tex=None, 
                 pos=LPoint2(0,0), 
                 size=LVector2(32,32),
                 order = 10,
                 transparency=True):
        obj = loader.loadModel("models/plane")
        NodePath.__init__(self, obj)
        self.reparentTo(camera)

        # Set the initial position and scale of the actor
        self.setPos(pos.getX(), 0.5, pos.getY())
        self.setScale(size.getX(), 1, size.getY())
        
        self.setVelocity(LVector2(0,0))
        self.age = 0
        self.lifetime= -1

        self.setDepthTest(False)

        if transparency:
            # Enable transparency blending
            self.setTransparency(TransparencyAttrib.MAlpha)

        if tex:
            # Load and set the requested texture
            img = loader.loadTexture("textures/" + tex)
            self.setTexture(img, 1)

        self.setBin("background", order)


    # sets the current position
    def setPosition(self, pos):
        newPos = LPoint3(pos.getX(), self.getPos().getY(), pos.getY())
        self.setPos(newPos)


    # return the current position
    def getPosition(self):
        pos3d = self.getPos()
        return LPoint2(pos3d.getX(), pos3d.getZ())


    # sets the current velocity
    def setVelocity(self, vel):
        self.velocity = vel


    # return the current position
    def getVelocity(self):
        return self.velocity


    def setLifetime(self, lt):
        self.lifetime = lt


    def isLifeExpired(self):
        return self.lifetime != -1 and self.age > self.lifetime


    def update(self, dt):
        # update age
        self.age = self.age + dt
        #update position based on velocity
        pos = self.getPosition()
        pos = pos + self.getVelocity() * dt
        self.setPosition(pos)
    

    def getSize(self):
        return LVector2(self.getScale().getX(), self.getScale().getZ())


    def collidesWith(self, other):
        # Get my position and other's position
        myPos = self.getPosition()
        otherPos = other.getPosition()

        # Get scales of both actors
        myScale = self.getSize()
        otherScale = other.getSize()

        # Determine if there is no overlap along the x-axis
        noXOverlap = (myPos.getX() - myScale.getX()/2) > (otherPos.getX() + otherScale.getX()/2) 
        if noXOverlap:
            return False
        noXOverlap = (myPos.getX() + myScale.getX()/2) < (otherPos.getX() - otherScale.getX()/2)
        if noXOverlap:
            return False
        # Determine if there is an overlap along the y-axis
        noYOverlap = (myPos.getY() - myScale.getY()/2) > (otherPos.getY() + otherScale.getY()/2) 
        if noYOverlap:
            return False
        noYOverlap = (myPos.getY() + myScale.getY()/2) < (otherPos.getY() - otherScale.getY()/2)
        if noYOverlap:
            return False

        # Since there is overlap along both axis...
        return True


    def kill(self):
        self.setLifetime(0)    # just expire lifetime