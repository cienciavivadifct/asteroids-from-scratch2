from gameactor import GameActor
from panda3d.core import LVector2

class Bullet(GameActor):
    
    BULLET_LIFE = 2
    
    def __init__(self, pos):
        GameActor.__init__(self, "bullet.png", pos, LVector2(4,4), 30)
        self.setLifetime(Bullet.BULLET_LIFE)
