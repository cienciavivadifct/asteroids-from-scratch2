# Developing 2D Games in Python with Panda3D

[Panda3D](http://www.panda3d.org) is a game engine for 3D rendering and game development for Python and C++ programs. Because it is a 3D game engine, and we are focused in developing 2D games, we will not use it directly and, instead, will use some in-house developed software that will interact with Panda3D and simplify things for us.

## Games are interactive programs

A game is an interactive application that responds to user input in real-time. While a large number of computer programs behave in a sequential pattern such as:

~~~~
Read data
Perform computations
Write results
~~~~

A computer game will typically be designed like this

~~~~
check for available hardware
load resources (images, sounds, 3D models, etc.)
while lives > 0 and game not completed
    load level
    while alive and level not completed 
        read input (keyboard, joystick, ...)
        process input (move player, fire, jump, display map, ...)
        display frame
        update all game actors
    end while
    if level completed 
        choose next level
    else 
        decrease lives
end while
game over   
~~~~

It is clear from the skeleton code above that a game will execute what is commonly called "*the game loop*" over and over again. This game loop is responsible for reading input from the controls, move the player and other game entities or game actors and update them all, getting them ready for the next loop iteration. The game loop needs to execute several dozens of times per second, otherwise the player will notice that the game is sluggish. Typically, 30 cycles per second is enough but 60 or 72 is even better.

One important aspect of this loop is that is contains the actual drawing part of the game, where the graphics are displayed on screen. A complete image (*frame*) is displayed in each iteration of the loop. So, we can also say that our game runs at 30 fps (*frames per second*).

## Game actors

One way to think of a game is by thinking that it is composed by several actors, each playing their own role. For instance, in the Pacman game, the player plays the role of Pacman and the computer is responsible for playing the roles of each monster:

- Blinky: the red ghost
- Pinky: the pink ghost
- Inky: the blue ghost
- Clyde: the orange ghost

![Alt](resources/ghost-names.png "Ghost names")
*Pacman ghost names*

We can also think of the food dots and power ups of Pacman in terms of actors, although these do not move and play a more passive role in the game.

In a sense, all actors are similar, they perform the same kind of tasks, each adding its own twist. In an object-oriented programming language, we can program what is similar in a base/parent class and program what is specific in a derived/sub class. So, all the actors in our game will be objects from a class called `GameActor`. For the Pacman game, we will probably also want to have a class named `Monster` to handle all the common behaviour to the monsters, and a specific class for each type of Monster: `Blinky`, `Inky`, `Pinky` and `Clyde`. Our hero, Pacman, will also be programmed in its own class, called `Pacman`. We can also think of a `Dot` or `Food` class to represent the dots that Pacman collects to earn points. We also want a `Powerup` class to represent the special "big" dots of food that give Pacman super powers and allow him to chase ghosts and eat them.

The classes that could be used to program the Pacman game are represented in the following hierarchy:

```
  GameActor
    +--- Pacman
    +--- Monster
    |      +--- Blinky
    |      +--- Inky
    |      +--- Pinky
    |      +--- Clyde
    +--- Dot
    +--- Poweup
```

## Controlling the actors

Like in a theatrical play, actors need to be conducted. Similarly to a stage director, a game also has a component that orchestrates the work performed by all the actors of the game. This director is the component that features the game loop.

In our case, we already provide a class called `Game2D`. This class should be used as a base class for our own class that will represent our game. So, we will need to program our Pacman game in a class, say `PacmanGame` that is derived from class `Game2D`. Here is the corresponding class hierarchy:

````
  Game2D
    |
    +--- PacmanGame
````

## Game Developer Survival Manual (Part I)

Here are the basic instructions that will let you survive through this course:

1. Create a new Python class to represent your game, for instance, `MyGame` in a file called `MyGame.py`.
2. Identify your different types of actors and plan a hierarchy of classes for them. All classes in the hierarchy will be directly or indirectly derived from `GameActor`.
3. Write each class in its own source file (with .py entension). Use lower case names for the filenames and capitalize class names.

# The Asteroids Game

![Alt](resources/AtariAsteroids.png "Asteroids game")
*screenshot from the original Atari Asteroids game*

[Asteroids](http://tinyurl.com/pok2ajt) was a highly popular game from Atari, released in 1979. The player controls a ship in an asteroid field. The purpose of the game is to destroy every single asteroid by firing to them. Asteroids disintegrate into smaller asteroids when hit and disappear if they are small enough.

## More on Actors and game loops

To program our games we will heavily rely on two classes: `GameActor` and `Game2D`. Our Asteroids game will need a subclass that has `Game2D` as its parent class.

Let's create a [asteroidsgame.py](asteroidsgame.py) file. First of all, since our `AsteroidsGame` class will be a subclass of `Game2D`, we need to inform that we will be using the `Game2D` class, from the `game2d` module (a module can contain several classes and is programmed in a single file):

```python
from game2d import Game2D
```

We can now declare our `AsteroidsGame` class, program its constructor and (an almost empty) game loop :

```python
from game2d import Game2D

class AsteroidsGame(Game2D):

    def __init__(self):
        Game2D.__init__(self)
        
    def gameLoop(self, task, dt):
        return Game2D.cont
```

The `__init__()` constructor simply calls the constructor from the parent class: `Game2D.__init__()`, with no extra arguments. 
The `gameLoop()` method takes several arguments. The most important one is the `dt` argument. This value represents the time that has elapsed since the last time the game loop was executed. For instance, a game that runs at 100 fps (*frames per second*) should execute the game loop method at nearly every 0.01 seconds. To keep the game running the `gameLoop()` method of our game needs to return a special value, represented by the `Game2D.cont` constant. There is another constant, `Game2D.done` that is used to end the game loop.

We are almost finished with our empty Asteroids game. We just need a way to start the game loop. For that, we will create an object from the `AsteroidsGame` class and ask it to run:

```python
class AsteroidsGame(Game2D):

    def __init__(self):
        Game2D.__init__(self)
        
    def gameLoop(self, task, dt):
        return Game2D.cont
        
mygame = AsteroidsGame()
mygame.run()
```

The `run()` method is simply an infinite loop that will end up calling the `gameLoop()` method on every iteration, as long as it continues to return Game2D.cont. In fact, `run()` will never end. But, if `gameLoop()` returns `Game2D.done`, it will no longer be called.

Note that if we run our game, we will only get an empty window. No action taking place, which is natural since we haven't used any actors.

## Let's take a peek at the Game2D class

If we look inside the [game2d.py](game2d.py) module, we see the following definition for `Game2D.__init__()`:

```python
    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
```

There are two additional parameters that we can pass to the `Game2D` class constructor: `width` and `height`. These are the dimensions, in pixels, of the window that will be created for our game. The parameters have default values, meaning that if we do not provide values for width and height, it will be exactly the same as providing `DEFAULT_WIDTH` and `DEFAULT_HEIGHT`, which are constants declared in [game2d.py](game2d.py), with values 800 and 600, respectively. The rest of the code in `Game2D.__init__()` handles the details of implementing a 2D projection of a 3D world (don't forget that Panda3D is a 3D game engine). Fortunately, we don't need to worry about it at all. Someone has taken care of the details for us and we can think in terms of 2D only. Near the end of the `Game2D.__init__()` constructor we see the following two lines:

```python
        self.width = width
        self.height = height
```

These are properties that all `Game2D` instances will store to record their window dimensions and that we can later use. Our `AsteroidsGame` instance is no different, since it is also a `Game2D` instance.

Immediately before those two lines there is an important line that has to do with the game loops of our games:

```python
        self.gameTask = taskMgr.add(self.topLevelGameLoop, "gameLoop")
```

Don't worry if you cannot grasp all the details but, in simplified way, what this line does is the creation of an independent task that is associated with the method `topLevelGameLoop()` from the `Game2D` class. If we look at the code for this method:

```python
    def topLevelGameLoop(self, task):
        # Get the delta time that has elapsed since we were last here
        dt = globalClock.getDt()
        
        # Pass the control to the game gameLoop() method 
        # and return whatever is returned from gameLoop()
        return self.gameLoop(task, dt)
```

We see that it simply gets the elapsed time (since our last time here) and pass it as an additional parameter to the `gameLoop()` method.

## Adding a background

The action of Asteroids takes place in space. Although space is mostly empty, wherever we look at we see lots of stars. It's time to add a background to our dull gray window.

### Drawing order
Game engines often have lots of components to draw on the screen at each frame. Some elements are part of the background, while others are drawn on top of the background and make up the core of the game actors. These can be either opaque or transparent. Finally, at the end, some elements like scores, lives, help system, instructions, etc. are displayed last, on top of everything.

To handle all this complexity, game engines usually put the objects to be drawn into special bins that are drawn in a specific order. In Panda3D we have the following bins available, listed by their drawing order:

- **background**: a bin for background objects. Objects inside this bin are drawn by increasing *sort* value.
- **opaque**: objects here are drawn from front to back order. Objects closer to the camera (greater Y values) are drawn first.
- **transparent**: objects here are drawn in back to front order (lower Y values first), with transparency enabled.
- **fixed**: objects in this bin are drawn in increasing *sort* value.
- **unsorted**: objects in this bin are drawn in order they are encountered.

When adding an object to the system we will need to specify its bin and its *sort* value, even if the specified bin doesn't use the *sort* value.

### Asteroids background
Our background image is stored in a file called [stars.jpg](textures/stars.jpg):

![stars.jpg](textures/stars.jpg)
*Asteroids starfield background*

We want the image to fully cover our window, ranging from (0,0), the lower left corner, to (width-1, height-1), the top right corner.

To achieve our purpose we are going to create a `GameActor`instance at the end of our `AsteroidsGame.__init__()` constructor:

```python
        # Our specific code goes here...
        bg = GameActor("stars.jpg", 
                       LPoint2(self.width/2,self.height/2), 
                       LVector2(self.width,self.height),
                       False)
```

Next, we add the newly created actor to the **background** bin, with a value of 10 for the sort order:

```python
        bg.setBin("background", 10)
```

We also need to add the following lines to the top of our [asteroidsgame.py](asteroidsgame.py) file:

```python
from gameactor import GameActor
from panda3d.core import LPoint2, LVector2
```
To better understand what is going with the creation of the game actor we need to look at the `GameActor`class, provided in [gameactor.py](gameactor.py). Let's do that immediately.

## The GameActor class

As we discussed before, a game actor is an entity of our game. It can be a ship, a rocket, an asteroid, or whatever entitiy we have in our game that plays some role in it.

The window of our 2D game is simply a rectangular grid of pixels. Each pixel can have its own color but, obviously, in a game, groups of neighboring pixels are related since they represent the same enitity: an asteroid, a ship, a rocket, etc.

Our `GameActor` objects are rectangles with arbitrary width and height whose center is placed at a certain location of our game window. The sides of this rectangle do not need to be aligned with window sides. This means that we can freely rotate an actor around its center. On this rectangle we decal an image, called a texture, that represents our actor. The following picture illustrates what has just been said:

![Alt](resources/gameactor.png "A game actor rectangle")
*A game actor in a game window*

It is now time to look at the parameters needed for the `GameActor.__init__()` constructor:

```python
class GameActor(NodePath):
    def __init__(self, 
                 tex=None, 
                 pos=LPoint2(0,0), 
                 size=LVector2(32,32),
                 transparency=True):
```

The constructor takes the following parameters:

- *tex*: the name of the file to be used as the texture for the actor
- *pos*: an LPoint2 (a point in 2D) specifying the location for the center of the actor on the screen
- *size*: an LVector2 (a vector in 2D) specifying the dimensions (in pixels) of the actor on the screen
- *transparency*: a boolean value that should be true if our texture includes transparent regions and false otherwise.

The complete code for the `GameActor.__init__()` method is listed below for illustration purposes. Don't worry if you don't understand completely what it does. You really don't need to. As long as you understand what you need to provide to the constructor, everything should be fine:

```python
class GameActor(NodePath):
    def __init__(self, 
                 tex=None, 
                 pos=LPoint2(0,0), 
                 size=LVector2(32,32),
                 transparency=True):
        obj = loader.loadModel("models/plane")
        NodePath.__init__(self, obj)
        self.reparentTo(camera)

        # Set the initial position and scale of the actor
        self.setPos(pos.getX(), 0.5, pos.getY())
        self.setScale(size.getX(), 1, size.getY())

        self.setDepthTest(False)

        if transparency:
            # Enable transparency blending
            self.setTransparency(TransparencyAttrib.MAlpha)

        if tex:
            # Load and set the requested texture
            img = loader.loadTexture("textures/" + tex)
            self.setTexture(img, 1)
```

The `GameActor` class also includes two additional methods to set and get the position of the actor:

```python
    # sets the current position
    def setPosition(self, pos):
        newPos = LPoint3(pos.getX(), self.getPos().getY(), pos.getY())
        self.setPos(newPos)

    # returns the current position
    def getPosition(self):
        pos3d = self.getPos()
        return LPoint2(pos3d.getX(), pos3d.getZ())
```

Don't forget that Panda3D is a 3D game engine and it operates in 3D space. These methods are what we need to forget 3D and focus on 2D only.

If we try to run our program now, we will see our background image filling our entire game window:

![Alt](resources/screenshot1.png "Screenshot of the game window with a starfield background")
*Screenshot of our game window with the background*

## Adding the ship

It's now time to add a ship to our Asteroids game. The ship image can be found in [ship.png](textures/ship.png):

![Alt](textures/ship.png "The Asteroids ship image")
*The asteroids ship texture*

We want our ship to be created in a position given when the ship is created and its size should be 16x16 pixels. We could create a `GameActor` just like we did for the background, with something like:

```python
    ship = GameActor("ship.png", LPoint2(x,y), LVector2(16,16))
```

However, our ship will have lots of special and unique features that distinguish it from the other game actors, like firing at asteroids, turning clockwise and anti-clockwise, thrusting, etc. These are more than enough reasons to create a new class to represent our ship. Even if only have one ship in our Asteroids game, having all the code that relates to the ship in its own class is a good principle.

Let's call the class `Ship` and place it in a file named [ship.py](ship.py). Here are the first lines:

```python
from gameactor import GameActor
from panda3d.core import LVector2

class Ship(GameActor):

    def __init__(self, pos):
        GameActor.__init__(self, "ship.png", pos, LVector2(16,16))
```

We will return to this class later to add new features, but for now this is enough.

So, we have a new `Ship` class and we want to add a `Ship` object to our game. We do this in the `AsteroidsGame.__init__()` method, just like we did to create the background:

```python
    def __init__(self):
        # We always need to call the __init__() function from the parent class.
        Game2D.__init__(self)
        # Our specific code goes here...
        self.createActors()
        
    ...
    
    def createActors(self):
        # create the background
        bg = GameActor("stars.jpg", 
                       LPoint2(self.width/2,self.height/2), 
                       LVector2(self.width,self.height),
                       False)
        bg.setBin("background", 10)
        
        # create the ship
        self.ship = Ship(LPoint2(self.width/2, self.height/2))
        self.ship.setBin("fixed", 40)
```

Ok, instead of adding the instruction to create the ship right into the `AsteroidsGame.__init__()` method, we put all the code that creates our game actors in a new method called `AsteroidsGame.createActors()` and called this method from `AsteroidsGame.__init__()`. The end result is the same, but our code is better organized. Programmers don't like to write long methods/functions and usually break a bigger/longer task into smaller ones. Each task should be programmed in a small method/function, usually with no more than 10-20 lines of code.

Note that our ship is placed in a different bin from the background. Objects from the **fixed** bin are drawn after the objects from the **background** bin, so our ship is always drawn on top of the background.

## Manoeuvering the ship

In the Asteroids game, the player controls the ship in three different ways:

- Thrust - the ship accelerates (with inercia) in the direction it is pointing at;
- rotate left - the ship rotates around itself, counterclockwise while keeping its movement in the same direction;
- rotate right - the ship rotates around itself clockwise while keeping its movement in the same direction;

As you can see, there is no braking in space! To break we need to accelerate in the direction opposite to our velocity. The solution is to turn the ship so that it moves backwards and then use the thrust to accelerate in the opposite direction. It takes some time to master the commands...

We will use the keys on the keyboard to control the ship with the following assignment:

- 'left arrow' - rotate left
- 'right arrow' - rotate right
- 'up arrow' - thrust
- 'space' - fire

And we will need to program three additional operations in our ship class:

- rotateLeft()
- rotateRight()
- thrust()
- fire()

Now, controlling the ship is simply a matter of connecting the keys to these three operations in the `Ship` class.

### Reading the keyboard in our Game2D

In Panda3D we can easily handle the input from the keyboard by assigning a function to be called whenever a key is pressed or released. For instance, we can terminate the program by calling a function named `sys.exit()`. 

Objects that inherit from the `ShowBase` class (directly or indirectly, such as our `Game2D` class), have a method called `accept()` that is used to process key down and key up events.

To call the `sys.exit()` function whenever we press the ESC key we could put the following line in our `AsteroidsGame.__init__()` constructor:

```python
        self.accept("escape", sys.exit)  # Escape quits
```

The first argument is the name of the event, in this case "escape" represents the action of the ESC key being pressed. For key releases we simply add "-up" to the name of the event, thus, "escape-up" would refer to the event of releasing the ESC key.

The second argument is the name of the function (note the absence of the parenthesis here since we are not calling the function yet) to be called whenever such event occurs.

It is possible to add a third argument to the accept function. This argument must be a list and their values will be the arguments to be passed to the function that we have associated with the event, whenever it occurs. 

For instance, let's suppose we have a general function `setKey()` that is called to handle most key events. Since it is not specific to a certain key, we will need to inform it which key has been pressed or released. The function can be a method of our `AsteroidsGame` class and it keeps the current state of each key using a dictionary, called `keys`. The dictionary keys are the the strings representing each keyboard key and the corresponding values are either true (key is pressed) or false (key is currently depressed):

```python
    def setKey(self, key, val):
        self.keys[key] = val
```

The first parameter (*key*) is the string representing the key, and the second parameter is either true or false, depending on whether we are handling a key press or a key release event. 

With the above method, we can now add the following lines to our `AsteroidsGame.__init__()` constructor to handle the user input:

```python
        # Initial state of the keys dictionary
        self.keys = {
            "left": 0, 
            "right": 0,
            "accel": 0, 
            "fire": 0
        }

        self.accept("escape", sys.exit)  # Escape quits
        # Other keys events set the appropriate value in our key dictionary
        self.accept("arrow_left",     self.setKey, ["left", 1])
        self.accept("arrow_left-up",  self.setKey, ["left", 0])
        self.accept("arrow_right",    self.setKey, ["right", 1])
        self.accept("arrow_right-up", self.setKey, ["right", 0])
        self.accept("arrow_up",       self.setKey, ["accel", 1])
        self.accept("arrow_up-up",    self.setKey, ["accel", 0])
        self.accept("space",          self.setKey, ["fire", 1])
        self.accept("space-up",       self.setKey, ["fire", 0])
```

To see if our handling of the controls is working, we can add a line in our game loop to print the keys dictionary. The `AsteroidsGame.gameLoop()` method will look like this:

```python
    def gameLoop(self, task, dt):
        print self.keys
        return Game2D.cont
```

We need one extra line near the top of our [asteroidsgame.py] file so that the `sys.exit()` function is recognized:

```python
import sys
```

If we now run our program, we still won't be able to control our ship, but we can press the appropriate keys and see the dictionary change the values for its keys reflecting what we did. Here is an example of the output written on the console:

```
{'fire': 0, 'rotRight': 0, 'accel': 0, 'rotLeft': 0}
...
{'fire': 0, 'rotRight': 0, 'accel': 1, 'rotLeft': 0}
...
{'fire': 0, 'rotRight': 0, 'accel': 0, 'rotLeft': 0}
...
{'fire': 0, 'rotRight': 1, 'accel': 0, 'rotLeft': 0}
...
{'fire': 0, 'rotRight': 0, 'accel': 0, 'rotLeft': 0}
...
{'fire': 1, 'rotRight': 0, 'accel': 0, 'rotLeft': 0}
...
```

### Rotating the ship

Now that we have confirmed that the keyboard is being handled correctly, let's tell the ship to rotate everytime we detect that the left or right arrow keys are down.

We need to do this in our `AsteroidsGame.gameLoop()` method:

```python
    def gameLoop(self, task, dt):
        if self.keys['left']:
            self.ship.rotateLeft(dt)
        elif self.keys['right']:
            self.ship.rotateRight(dt)
 
        return Game2D.cont
```

You can see that our previous print command that we had inserted is no longer necessary. An interesting fact in the code above is that we are instructing the ship to rotate (left or right) by an ammount that is dependent on the elapsed time from the last frame. The idea is to keep a constant rotation rate. If the computer takes more time between two consecutive steps the value of dt will be higher and so will the rotation.

Now we need to change the `Ship` class to handle the new operations. Rotating right and rotating left should be pretty similar. In fact on can be implemented using the other by using a negative angle: turing left is like turning right a negative value. We opted to implement a `rotate()` operation and have both `rotateLeft()` and `rotateRight()` use that operation:

```python
    def rotateLeft(self, dt):
        self.rotate(-dt*Ship.ROTATION_SPEED)
        
    def rotateRight(self, dt*Ship.ROTATION_SPEED):
        self.rotate(ammount)
```

Note that we have multiplied the elapsed time by a constant (`Ship.ROTATION_SPEED`) to fine tune the angle that the ship rotates per unit time.

The `Ship.ROTATION_SPEED` constant is declared inside the `Ship` class, but outside any method:

```python
...
class Ship(GameActor):

    ROTATION_SPEED = 200
...
```

Now, we have to implement the `rotate()` operation. Panda3d uses Heading/Pitch/Roll angles to rotate an object. Here, we are interested in rotations around the Y axis and that means the Roll angle in Panda3D. Panda3D offers the `getR()` and `setR()` operations to modify this angle in instances of the `ShowBase` class (or its derivatives like GameActor or Ship). Our `rotate()` method can be something like:

```python
    def rotate(self, ammount):
        # get current adding
        heading = self.getR()
        # compute new value
        heading += ammount
        # set the new value
        self.setR(heading % 360)
```

It's time to see the ship spinning in space. Run the program!