import sys
from game2d import Game2D
from gameactor import GameActor
from panda3d.core import LPoint2, LVector2
from random import choice
from ship import Ship
from asteroid import Asteroid
from direct.gui.OnscreenText import OnscreenText

class AsteroidsGame(Game2D):
    """
    Class for our AsteroidsGame.
    """

    ASTEROIDS_PER_LEVEL = 10
    SAFE_MARGIN = 50

    def __init__(self):
        # We always need to call the __init__() function from the parent class.
        Game2D.__init__(self)
        # Our specific code goes here...
        self.createActors()
        self.createScore()
        self.loadSounds()
        
        # Initial state of the keys dictionary
        self.keys = {
            "left": 0,
            "right": 0,
            "accel": 0,
            "fire": 0
        }
        
        self.accept("escape", sys.exit)  # Escape quits
        # Other keys events set the appropriate value in our key dictionary
        self.accept("arrow_left",     self.setKey, ["left", 1])
        self.accept("arrow_left-up",  self.setKey, ["left", 0])
        self.accept("arrow_right",    self.setKey, ["right", 1])
        self.accept("arrow_right-up", self.setKey, ["right", 0])
        self.accept("arrow_up",       self.setKey, ["accel", 1])
        self.accept("arrow_up-up",    self.setKey, ["accel", 0])
        self.accept("space",          self.setKey, ["fire", 1])
        self.accept("space-up",       self.setKey, ["fire", 0])


    def gameLoop(self, dt):
        if self.keys['left']:
            self.ship.rotateLeft(dt)
        elif self.keys['right']:
            self.ship.rotateRight(dt)
        if self.keys['accel']:
            self.ship.thrust(dt)
        if self.keys['fire']:
            self.sndFire.play()
            self.ship.fire(self)
            self.keys['fire']=0

        for a in self.actors:
            self.adjustPosition(a)
            
        # Handle collisions
        self.handleCollisions()
        
        if self.ship.isLifeExpired():    # no ship, no game, no fun :-(
            self.gameOverMsg()
            return Game2D.done
        else:
            if len(self.actorsWithRole("asteroid")) == 0 \
                    and len(self.actorsWithRole("bullet")) == 0:
		self.createAsteroids()   # resart level
            return Game2D.cont


    def handleCollisions(self):
        allBullets = self.actorsWithRole('bullet')
        allAsteroids = self.actorsWithRole('asteroid')

        # Handle collisions between bullets and the asteroids
        for b in allBullets:
            for a in allAsteroids:
                if b.collidesWith(a):
                    self.addScore(a.points)
                    self.sndExpl.play()
                    a.kill(self)
                    b.kill()

        # Handle collisions between ship and asteroids
        for b in allAsteroids:
            if self.ship.collidesWith(b):
                self.sndExpl.play()
                self.ship.kill()


    def adjustPosition(self, a):
        pos = a.getPosition()
        px = pos.getX() % self.width
        py = pos.getY() % self.height
        a.setPosition(LPoint2(px, py))
    

    def createActors(self):
        self.createBackground()
        self.createShip()
        self.createAsteroids()


    def createBackground(self):
        # create the background
        bg = GameActor("stars.jpg", 
                       LPoint2(self.width/2,self.height/2), 
                       LVector2(self.width,self.height),
                       10,
                       False)        


    def createShip(self):
        # create the ship
        self.ship = Ship(LPoint2(self.width/2, self.height/2))
        self.addActor(self.ship, ['ship'])


    def getRandomPosition(self):
        x = choice(
            range(0,self.width/2-AsteroidsGame.SAFE_MARGIN) + 
            range(self.width/2+AsteroidsGame.SAFE_MARGIN, self.width)
        )
        y = choice(
            range(0,self.height/2-AsteroidsGame.SAFE_MARGIN) + 
            range(self.height/2+AsteroidsGame.SAFE_MARGIN, self.height)
        )
        return LPoint2(x,y)


    def createAsteroids(self):
        for i in range(0,AsteroidsGame.ASTEROIDS_PER_LEVEL):
            asteroid = Asteroid(self.getRandomPosition())
            self.addActor(asteroid, ['asteroid'])


    def setKey(self, key, val):
        self.keys[key] = val


    def gameOverMsg(self):
        self.scoreboard = OnscreenText("GAME OVER", pos=(400, 300),
            parent=camera, fg=(1,0.1,0.1,1), scale=60 )


    def createScore(self):
        self.score = 0
        self.scoreboard = OnscreenText("Score: 0", parent=camera, pos=(50, 570), fg=(1,1,1,1), scale=18 )


    def addScore(self, points):
        self.score = self.score + points
        self.scoreboard.setText("Score: "+str(self.score))
    

    def loadSounds(self):
        # load sound efects (sfx)
        self.sndExpl = loader.loadSfx("sounds/Die.ogg")  # other formats can be used
        self.sndFire = loader.loadSfx("sounds/Lazer.ogg")


# Let us create an object from our AsteroidsGame class...
game = AsteroidsGame()
# ... and ask it to run
game.run()
