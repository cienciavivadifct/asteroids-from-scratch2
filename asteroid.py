from gameactor import GameActor
from random import random, choice
from math import sin, cos, pi

from panda3d.core import LVector2

class Asteroid(GameActor):
    
    INITIAL_SPEED = 20
    MIN_SIZE = 30
    SIZE_STEP = 0.6
    VEL_SCALE = 1.9
    POINTS = 1
    POINTS_SCALE = 2

    def __init__(self, pos):
        GameActor.__init__(self, 
                           choice(["asteroid1.png", "asteroid2.png", "asteroid3.png"]),
                           pos, LVector2(60,60), 
                           25)
        
        heading = random() * 2 * pi
        v = LVector2(sin(heading), cos(heading)) * Asteroid.INITIAL_SPEED
        self.setVelocity(v)
        self.points = Asteroid.POINTS


    def kill(self, game):
        GameActor.kill(self)
        # Check if the asteroid is big enough and if so, create two new ones
        if self.getScale().getX() > Asteroid.MIN_SIZE:
            # create a velocity vector perpendicular to the original one
            vel = LVector2(self.getVelocity().getY(), -self.getVelocity().getX())
            # scale up its velocity (smaller fragments move faster!)
            vel = vel * Asteroid.VEL_SCALE
            # scale down its size
            size = self.getScale() * Asteroid.SIZE_STEP
            
            # Create a first asteroid
            newAsteroid = Asteroid(self.getPosition())
            newAsteroid.setVelocity(vel)
            newAsteroid.setScale(size)
            newAsteroid.points = self.points * Asteroid.POINTS_SCALE
            game.addActor(newAsteroid, ["asteroid"])

            # Create a second asteroid
            newAsteroid = Asteroid(self.getPosition())
            newAsteroid.setVelocity(vel * -1)
            newAsteroid.setScale(size)
            newAsteroid.points = self.points * Asteroid.POINTS_SCALE
            game.addActor(newAsteroid, ["asteroid"])