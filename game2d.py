from direct.showbase.ShowBase import ShowBase
from panda3d.core import OrthographicLens
from direct.task import Task

class Game2D(ShowBase):
    """
    A class for 2D Games in Python.

    This class is a wrapper class around ShowBase class from Panda3D.
    Panda3D is a framework for 3D games and applications. Our 2D games use only 2D (x,y) coordinates.
    However, Panda3D only cares about 3D worlds, so we do the following conversion:

        Game2D horizontal (x) direction is Panda3D horizontal (x) direction
        Game2D vertical (y) direction is Panda3D vertical (z) direction

        Panda3D y direction is not used and points to our eyes.
    """

    # Default width and height for game windows
    DEFAULT_WIDTH = 800
    DEFAULT_HEIGHT = 600

    # Default depth range for visible objects
    NEAR_VAL = 0
    FAR_VAL = 1

    # Constants returned by gameLoop
    cont = Task.cont
    done = Task.done

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        ShowBase.__init__(self)

        # We use an Orthographic projection for 2D
        lens = OrthographicLens()

        # And set the film size to work with pixel units,
        # or whatever is appropriate for your scene
        lens.setFilmSize(width, height)

        # Adjust the offset so that bottom-left corner is at (0,0)
        # and top-right at (SCREEN_WIDTH,SCREEN_HEIGHT)
        lens.setFilmOffset(width/2,height/2)

        # Near and far plane values are adjusted to include Y=0
        lens.setNearFar(Game2D.NEAR_VAL, Game2D.FAR_VAL)

        # Updates the camera node of our scence
        base.cam.node().setLens(lens)

        self.gameTask = taskMgr.add(self.topLevelGameLoop, "gameLoop")

        self.width = width
        self.height = height
        
        # The container for all actors that need to be updated in gameLoop
        self.actors = []
        self.roles = {}

    # Used to add an actor to the actors list
    def addActor(self, a, roles=[]):
        # add the actor to the actors list regardless of its roles
        self.actors.append(a)
        # for each role, add the actor to the appropriate list (stored in the roles dictionary)
        for r in roles:
            self.addActorWithRole(a, r)
        

    # Add an actor with a single role
    def addActorWithRole(self, a, r):
        # Check if the role has already been used
        if not r in self.roles.keys():
            # Add an empty list for the new role
            self.roles[r] = []

        # Add the actor to the list stored with the r key
        self.roles[r].append(a)


    # Returns the list of actors with a specific role
    def actorsWithRole(self, r):
        if not r in self.roles.keys():
            return []
        else:
            return self.roles[r]


    # Removes an actor from each role's list and from the global list
    def removeActor(self, a):
        # remove actor from the actors' list
        self.actors.remove(a)

        # Go through each role
        for r in self.roles.keys():
            # Test if actor is in the list of this role...
            if a in self.roles[r]:
                # ... if yes, remove it
                self.roles[r].remove(a)

        # remove the actor from Panda3D
        a.removeNode()


    def topLevelGameLoop(self, task):
        # Get the delta time that has elapsed since we were last here
        dt = globalClock.getDt()

        # Update all actors

        for a in self.actors:
            if(a.isLifeExpired()):
                self.removeActor(a)
            else:
                a.update(dt)

        # Pass the control to the game gameLoop() method and return whatever is returned from gameLoop()
        return self.gameLoop(dt)


    # game loop for a specific game. Derived classes will probably override this to
    # do whatever is needed in a particular game. We simply return Game2D.cont to signal
    # that the game is to continue running
    def gameLoop(self, dt):
        return cont
